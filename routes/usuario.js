const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).send({
        message: 'lista usuarios'
    });
});





router.post('/', (req, res, next) => {
    let usuarioEntrada = {
        nome: req.body.nome,
        idade: req.body.idade
    };

    res.status(200).send({
        message: 'Usuario salvo com sucesso',
        user: usuarioEntrada
    });
});

router.get('/:id_usuario', (req, res, next) => {
    let idUsuario = req.params.id_usuario;
    res.status(200).send({
        message: 'usuario de id :',
        id: idUsuario
    });


});

module.exports = router;