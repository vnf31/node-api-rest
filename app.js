const express = require('express');
const app = express(); 
const morgan = require('morgan');
 const bodyParser = require('body-parser')

const rotasUsuario = require('./routes/usuario')
const rotasProduto = require('./routes/produto')

app.use(morgan('dev'));
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.use('/produtos', rotasProduto);
app.use('/usuario', rotasUsuario);

app.use((req, res, next) => {
    const error = new Error ('Nao encontrado!');
    error.status(404);
    next(errors);
});


app.use((error, req,res, next) => {
    res.status(error.status || 500);
    return res.send({
        error : {
            message : error.message
        }
    })
})


module.exports = app;