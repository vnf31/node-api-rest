create schema app_node;
 
 use app_node;
 
create table usuario(
	id int not null auto_increment primary key,
	nome varchar (50),
	telefone varchar(20),
	idade int 
);

insert into usuario(nome, telefone, idade) values('cassio', '(47) 99999 1111', 21);

select * from usuario;